<?php
require_once("connexion.inc.php");
class Todo{
	private $request;
	private $params;
	private $connexion;
	
	function __construct($request=null,$params=null){
		$this->request=$request;
        $this->params=$params;
	}
	
	function getConnexion(){
        $myConnexion = new Connexion("192.168.1.18", "twitter", "password", "todo");
        $myConnexion->connect();
		return $myConnexion->getConnexion();
	}

	function execute(){
		$this->connexion = $this->getConnexion();
		$stmt = $this->connexion->prepare($this->request);
		$stmt->execute($this->params);
		$this->disconnect();
		return $stmt;		
	}
	function disconnect(){
		unset($this->connexion);
	}
	
}//end of Todo class
?>