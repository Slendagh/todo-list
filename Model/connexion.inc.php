<?php
class Connexion{
	private $server;
	private $user;
	private $password;
	private $dataBase;
	private $connexion;
	
	function __construct($server, $user, $password, $dataBase){
		$this->server=$server;
		$this->user=$user;
		$this->password=$password;
		$this->dataBase=$dataBase;
	}
	
	function getConnexion(){
		return $this->connexion;
	}
	
	function connect(){
	   try {
            $dns = "pgsql:host=$this->server;port=5432;dbname=$this->dataBase;user=$this->user;password=$this->password";
            $options = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            );
            $this->connexion = new PDO($dns, $this->user, $this->password, $options);
		} catch ( Exception $e ) {
			echo $e->getMessage();
			echo "Problem connecting to server";
			exit();
		}
	}	
}//end of Connexion class
?>