<?php
include_once("../Model/todo.inc.php");
include_once("../Controller/todoController.php");
include_once("../style.css");
?>
<!Doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Todo List</title>
    </head>
    <body>
        <style>
            html, body{
                font-family: Arial, Helvetica, sans-serif;
            }
            .container{
                display:flex;
                flex-direction:column;
                align-items:center;
            }

            ul li{
                list-style-type: none;
                margin:10px;
            }

            li{
                display:flex;
                flex-direction:row;
            }

            ul li.complete{
                text-decoration: line-through;
                color:gray;
            }

            .task{
                width:180px;
                cursor: pointer;
            }

            .done{
                margin-left:10px;
                background-color:#4CA64C;
                border:none;
                padding:6px;
                text-decoration:none;
                cursor:pointer;
                border-radius:5px;
            }
            .unDone{
                margin-left:10px;
                background-color:gray;
                border:none;
                padding:6px;
                text-decoration:none;
                cursor:pointer;
                border-radius:5px;
            }
            .delete{
                margin-left:10px;
                background-color:#FF3232;
                border:none;
                padding:6px;
                text-decoration:none;
                cursor:pointer;
                border-radius:5px;
            }

            .cadre{
                border:1px solid #FF4703;
                width:25%;
                display:flex;
                flex-direction:column;
                align-items:center;
                border-radius: 15px 50px;
                padding: 20px;
            }

            .add{
                background-color:#FF4703;
                border:none;
                padding:5px;
                text-decoration:none;
                cursor:pointer;
                width:50px;
                border-radius:5px;
            }
            input[type=text]{
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                width:50%;
            }
            .enterTask{
                width:100%;
                text-align:center;
                margin-left:10px;
            }
            ul{
                padding-left:10px;
            }
        </style>

        <div class="container">
        <div class="cadre">
            <h2>Todo List</h2>
            <form class="enterTask" method="post">
                <input type="text" name="description" value="">
                <input class="add" type="submit" name="submit" value="add">
            </form>
            <ul id="todo_list">
                <?php
                    $request = "select * from todolist";
                    $todoObj = new Todo($request, null);
                    $data = $todoObj->execute()->fetchAll(PDO::FETCH_BOTH);
                    foreach($data as $row){
                            if ($row['complete'] != true) {
                                    ?>
                                <li>
                                    <span class="task"><?=htmlspecialchars($row['description'])?></span>
                                    <form method="POST">
                                    <button class="done" type="submit" name="complete">done</button>
                                    <input type="hidden" name="id" value="<?=$row['id']?>">
                                    <input type="hidden" name="complete" value="true">
                                    </form>
                                <?php
                            } else {
                                    ?>
                                <li class="complete">
                                    <span class="task"><?=htmlspecialchars($row['description'])?></span>
                                    <form method="POST">
                                    <button class="unDone" type="submit" name="unComplete">undo</button>
                                    <input type="hidden" name="id" value="<?=$row['id']?>">
                                    <input type="hidden" name="unComplete" value="true">
                                    </form>
                                <?php
                            }
                            ?>
                            <form method="POST">
                                <button class="delete" type="submit" name="delete">X</button>
                                <input type="hidden" name="id" value="<?=$row['id']?>">
                                <input type="hidden" name="delete" value="true">
                            </form>
                            </li>
                <?php
                    }
                ?>
            </ul>
        </div>
</div>


    </body>

</html>
