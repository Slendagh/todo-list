<?php
include_once("../Model/todo.inc.php");

if(isset($_POST['submit'])) 
{
    $description = $_POST['description'];
    $request = "INSERT INTO todolist (description) VALUES (?)";
    $todoObj = new Todo($request, array($description));
    $data = $todoObj->execute();
   
}

# Delete TODO
elseif (isset($_POST['delete']))
{ 
    $id = $_POST['id'];
    $request = "delete from todolist where id = ?";
    $todoObj = new Todo($request, array($id));
    $data = $todoObj->execute();
}

# Update completion status
elseif (isset($_POST['complete']))
{
    $id = $_POST['id'];
    $request = "UPDATE todolist SET complete =? where id =?";
    $todoObj = new Todo($request, array(true, $id));
    $data = $todoObj->execute();
}

elseif (isset($_POST['unComplete']))
{
    $id = $_POST['id'];
    $request = "UPDATE todolist SET complete =? where id =?";
    $todoObj = new Todo($request, array(null, $id));
    $data = $todoObj->execute();
}